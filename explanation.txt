sudo apt install git


sudo apt install vim


mkdir Basic_GIT_Linux


cd Basic_GIT_Linux


git init
הופך את התיקייה הנוכחית לתיקיית  git, ויוצר את ה-git repository 


vim contents.py
(בשלב זה לחצתי 'i' בשביל לערוך, הדבקתי את הקוד שכתבתי ולסיום לחצתי על 'esc' ולאחר מכן כתבתי 'wq:' ולחצתי enter על מנת לשמור ולסגור את הקובץ)


chmod 740 contents.py


git remote add remote_repo https://itayo252@bitbucket.org/itayo252/Basic_GIT_Linux.git
מוסיף את ה-upstream של הקישור הנ"ל בשם origin, וכך אפשר לבצע פעולות ב- git repositorie המרוחק 


git config --global user.email "itayo252@gmail.com"
מגדיר את האימייל של המשתמש


git config --global user.name "Itay Peleg"
מגדיר את השם של המשתמש


git add contents.py
מוסיף את קובץ הפייתון ל-staging area


git commit –m "First commit"
יוצר קומיט חדש בשם "First commit"


git push –u remote_repo master
"מעלה" את הקומיט הנוכחי עליו מצביע הענף (branch) "master" אל ה-upstream שהוגדר בשם origin.

